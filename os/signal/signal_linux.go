package signal

import (
	"os"
	"os/signal"
	"syscall"
)

func NotifySignalTerminate() chan os.Signal {
	s := make(chan os.Signal, 1)

	signal.Notify(s, syscall.SIGINT, syscall.SIGTERM, syscall.SIGPIPE)

	return s
}
