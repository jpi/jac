.PHONY: all tools
.DEFAULT: tools

all: tools

JPI_BASE_URI = go.jpi.io
JAC_URI = $(JPI_BASE_URI)/jac

SERVER_BIN = $(GOBIN)/jacd
SERVER_ARGS = -P 2200 -p 7070

SOURCES = $(shell find src/$(JAC_URI) -name '*.go' -type f)

# amery/docker-go-builder
#
.PHONY: fmt run run_server trace_server

fmt: gofmt
gofmt: $(SOURCES)

run: run_server

include docker-go.mk

tools: FORCE
	$(GOGET) $(JAC_URI)/cmds/...

run_server: $(SERVER_BIN) ssh_keygen
	$< $(SERVER_ARGS)

trace_server: PORT=6060
trace_server: TRACE_OUT=trace.out
trace_server: $(SERVER_BIN) ssh_keygen
	$< -T $(SERVER_ARGS) 3> $(TRACE_OUT)
	DOCKER_EXPOSE=$(PORT) $(GO) tool trace -http=:$(PORT) $(TRACE_OUT)

ssh_keygen: FORCE
	@mkdir -p "$(WS)/conf"; \
	for x in rsa dsa ecdsa ed25519; do \
		f="$(WS)/conf/ssh_host_$${x}_key"; \
		[ -s "$$f" ] || ssh-keygen -f "$$f" -N '' -t $$x; \
	done

$(GOBIN)/jacd: FORCE
	$(GOGET) $(JAC_URI)/cmds/$(notdir $@)
