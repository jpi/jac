package main

import (
	"github.com/pborman/getopt/v2"
	"go.jpi.io/jac/client"

	"fmt"
	"os"
)

func main() {
	c := &client.Config{}
	_ = c

	if err := getopt.Getopt(nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
