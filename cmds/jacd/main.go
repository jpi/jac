package main

import (
	"github.com/pborman/getopt/v2"
	"go.jpi.io/jac/os/signal"
	"go.jpi.io/jac/server"

	"fmt"
	"os"
	"runtime/trace"
)

func main() {
	c := &server.Config{}

	// getopt
	httpsPort := getopt.Uint16Long("https-port", 'p', 0, "port to listen for https connections")
	sshPort := getopt.Uint16Long("ssh-port", 'P', 0, "port to listen for ssh connections")
	sshKeysDir := getopt.StringLong("ssh-keys-dir", 'C', "conf", "directory containing ssh_host_*_key files")
	doTrace := getopt.BoolLong("trace", 'T', "use runtime/trace to fd:3")

	if err := getopt.Getopt(nil); err != nil {
		getopt.SetParameters("")

		fmt.Fprintln(os.Stderr, err)
		getopt.PrintUsage(os.Stderr)
		os.Exit(1)
	}

	if *doTrace {
		out := os.NewFile(3, "trace.out")
		trace.Start(out)
		defer trace.Stop()
	}

	if *sshPort > 0 {
		c.SSHPort = *sshPort
	}

	if *httpsPort > 0 {
		c.Port = *httpsPort
	}

	if len(*sshKeysDir) > 0 {
		c.SSHKeysDirectory = *sshKeysDir
	}

	c.SetDefaults()

	srv := server.NewServer(c, logger.Prefix())

	// Channels
	sigterm := signal.NotifySignalTerminate()
	done := make(chan error, 1)

	// Run
	go func() {
		err := srv.Serve()
		logE("err: %v", err)
		done <- err
	}()

	for {
		select {
		case err := <-done:
			logE("err: %v", err)
			goto exit
		case s := <-sigterm:
			logE("sigterm: %v", s)

			err := fmt.Errorf("Terminated by %s", s)
			srv.Terminate(err)
		}
	}

exit:
	logI("Exiting")
}
