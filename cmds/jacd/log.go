package main

import (
	"go.jpi.io/jac/log"
)

const (
	NInfo log.Variant = iota
	NTrace
	NDebug
	NError
)

var logger = log.New("jacd: ", log.Lrelative|log.Lmicroseconds|log.Lelapsed).
	SetVariant(NTrace, "T/", log.Lor|log.Lpackage|log.Lfunc|log.Lshortfile|log.Lfileline).
	SetVariant(NDebug, "D/", log.Lor|log.Lpackage|log.Lfunc).
	SetVariant(NError, "E/", log.Lor|log.Lpackage)

func logT(fmt string, args ...interface{}) {
	logger.Outputf(1, NTrace, fmt, args...)
}

func logD(fmt string, args ...interface{}) {
	logger.Outputf(1, NDebug, fmt, args...)
}

func logI(fmt string, args ...interface{}) {
	logger.Outputf(1, NInfo, fmt, args...)
}

func logE(fmt string, args ...interface{}) {
	logger.Outputf(1, NError, fmt, args...)
}
