package parser

import (
	"strings"
)

type RuneAccepter func(rune) bool
type StringAccepter func(string) (int, bool)

//
func NewStringAccepter(args ...string) StringAccepter {
	if len(args) == 0 {
		// nothing to accept
		return func(_ string) (int, bool) {
			return 0, false
		}
	}

	// report minimum length required when failing
	min := -1
	for _, s1 := range args {
		l1 := len(s1)
		if min < 0 || l1 < min {
			min = l1
		}
	}

	return func(s0 string) (int, bool) {
		if l0 := len(s0); l0 >= min {
			for _, s1 := range args {
				if l1 := len(s1); l0 >= l1 {
					if strings.HasPrefix(s0, s1) {
						return l1, true
					}
				}
			}
		}
		return min, false
	}
}

func AcceptString(data string, accept StringAccepter) (int, string, bool) {
	if l, ok := accept(data); ok {
		return l, data[:l], true
	} else {
		return 0, data[:0], false
	}
}

func FindString(data string, accept StringAccepter) (int, string, bool) {
	l0 := len(data)
	for i := 0; i < l0; i++ {
		if l, ok := accept(data[i:]); ok {
			return i, data[i : i+l], true
		} else if i+l >= l0 {
			// abort, next iteration won't be long enough anyway
			break
		}
	}

	return 0, data[:0], false
}

func AcceptToken(data string, atEOF bool, accept RuneAccepter) (int, string, bool) {
	for i, c := range data {
		if accept(c) {
			// next!
		} else if i == 0 {
			// nothing
			goto fail
		} else {
			// token complete
			return i, data[:i], true
		}
	}

	if len(data) > 0 && atEOF {
		// accept it all
		return len(data), data, true
	}
fail:
	return 0, "", false
}
