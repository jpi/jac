package parser

func StringInSlice(a string, list []string) int {
	for i, b := range list {
		if a == b {
			return i
		}
	}
	return -1
}
