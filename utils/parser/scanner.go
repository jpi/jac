package parser

import (
	"bufio"
	"io"
)

type ScanSplitHandler interface {
	Split(data []byte, atEOF bool) (int, []byte, error)
	Handler(string, error) error
}

type Scanner struct {
	h ScanSplitHandler
	s *bufio.Scanner
}

func (p *Scanner) split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var l int

	for len(data) > 0 && token == nil && err == nil {
		l, token, err = p.h.Split(data, atEOF)

		advance += l
		data = data[l:]
	}

	return
}

func (p *Scanner) Run() (err error) {
	for p.s.Scan() {
		if err = p.h.Handler(p.s.Text(), nil); err != nil {
			return
		}
	}

	if err = p.s.Err(); err == nil {
		err = io.EOF
	}

	return p.h.Handler("", err)
}

// NewScanner creates a bufio.Scanner wrapper
func NewScanner(f io.Reader, h ScanSplitHandler) *Scanner {
	p := &Scanner{
		h: h,
		s: bufio.NewScanner(f),
	}

	p.s.Split(p.split)
	return p
}
