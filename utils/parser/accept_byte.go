package parser

type ByteAccepter func(c byte) bool

// AcceptByte tells if a given byte is acceptable
func AcceptByte(c byte, f ByteAccepter) bool {
	return f(c)
}

// AcceptManyBytes tells how long of a prefix is acceptable
func AcceptManyBytes(data []byte, f ByteAccepter) (int, bool) {
	if l := len(data); l > 0 {
		for i, c := range data {
			if !f(c) {
				return i, (i > 0)
			}
		}
		return l, true
	} else {
		return 0, false
	}
}

// ByteInString
func NewByteInStringAccepter(s string) ByteAccepter {
	if len(s) > 0 {
		s1 := []byte(s)

		return func(c byte) bool {
			for _, c1 := range s1 {
				if c1 == c {
					return true
				}
			}
			return false
		}
	}

	return func(_ byte) bool {
		return false
	}
}

func AcceptByteInString(a byte, set string) bool {
	return AcceptByte(a, NewByteInStringAccepter(set))
}

func AcceptManyBytesInString(data []byte, set string) (int, bool) {
	return AcceptManyBytes(data, NewByteInStringAccepter(set))
}

//
func AcceptStringBytes(data []byte, accept StringAccepter) (int, []byte, bool) {
	if l, _, ok := AcceptString(string(data), accept); ok {
		return l, data[:l], true
	}

	return 0, []byte{}, false
}

//
func AcceptTokenBytes(data []byte, atEOF bool, accept RuneAccepter) (int, []byte, bool) {
	if l, _, ok := AcceptToken(string(data), atEOF, accept); ok {
		return l, data[:l], true
	}

	return 0, []byte{}, false
}

func FindStringBytes(data []byte, accept StringAccepter) (int, []byte, bool) {
	if l, _, ok := FindString(string(data), accept); ok {
		return l, data[:l], true
	}

	return 0, []byte{}, false
}
