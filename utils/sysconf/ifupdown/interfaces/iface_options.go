package interfaces

import (
	"sort"
)

type ifaceOptions struct {
	iface   *Iface
	options map[string][]string
}

func newIfaceOptions(iface *Iface) ifaceOptions {
	return ifaceOptions{
		iface:   iface,
		options: make(map[string][]string),
	}
}

func (p *ifaceOptions) Names() []string {
	names := make([]string, len(p.options))[:0]
	for k, _ := range p.options {
		names = append(names, k)
	}

	sort.Strings(names)
	return names
}

func (p *ifaceOptions) CopyTo(out *ifaceOptions) {
	for k, v := range p.options {
		out.options[k] = v
	}
}

func (p *ifaceOptions) Add(key string, values ...string) error {
	p.options[key] = append(p.options[key], values...)
	return nil
}

func (p *ifaceOptions) Set(key string, values ...string) error {
	p.options[key] = values
	return nil
}

func (p *ifaceOptions) Get(key string) ([]string, bool) {
	v, ok := p.options[key]
	return v, ok
}

func (p *ifaceOptions) Remove(key string) {
	delete(p.options, key)
}
