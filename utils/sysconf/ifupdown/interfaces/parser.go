package interfaces

import (
	"go.jpi.io/jac/utils/parser"

	"io"
	"log"
)

type Parser struct {
	*parser.Scanner

	d parserData
}

func (p *Parser) Handler(text string, err error) error {
	if len(text) == 0 && err == io.EOF {
		err = p.d.ProcessLine()
	} else if err == nil {
		if text == "\n" || text == "\r\n" {
			err = p.d.ProcessLine()
		} else if len(text) > 0 {
			err = p.d.ProcessText(text)
		}
	}

	if err != nil {
		log.Printf("err:%s", err)
	}

	return err
}

func (p *Parser) Run() (*Interfaces, error) {
	if err := p.Scanner.Run(); err != nil {
		return nil, err
	} else {
		p.d.Reduce(nil)
		return p.d.Export(nil)
	}
}

//
func NewParser(f io.Reader) *Parser {
	p := &Parser{
		d: newParserData(),
	}

	p.Scanner = parser.NewScanner(f, p)
	return p
}
