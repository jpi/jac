package interfaces

import (
	"go.jpi.io/jac/utils/parser"

	"bufio"
	"fmt"
)

var (
	commandOptions = []string{"pre-up", "up", "post-up", "pre-down", "down", "post-down"}
)

type parserData struct {
	Split bufio.SplitFunc
	q     []string

	ifaces    Interfaces
	lastIface *Iface
}

func newParserData() parserData {
	return parserData{
		Split:  splitToken,
		ifaces: NewInterfaces(),
	}
}

//
func (d *parserData) AddOption(iface *Iface, key string, values ...string) error {
	if iface == nil {
		return d.ifaces.AddOption(key, values...)
	} else {
		return iface.AddOption(key, values...)
	}
}

//
func (d *parserData) ProcessLine() (err error) {
	if len(d.q) > 0 {
		if d.q[0] == "auto" {
			err = d.AddOption(nil, d.q[0], d.q[1:]...)
		} else if d.q[0] == "iface" {
			var p *Iface

			if len(d.q) != 4 {
				err = fmt.Errorf("%q: Invalid iface definition", d.q)
			} else if p, err = d.ifaces.New(d.q[1], d.q[2], d.q[3]); err == nil {
				d.lastIface = p
			}
		} else {
			err = d.AddOption(d.lastIface, d.q[0], d.q[1:]...)
		}
	}

	// Prepare for next line
	d.Split = splitToken
	d.q = d.q[:0]
	return err
}

func (d *parserData) ProcessText(text string) error {
	if len(text) > 0 && text[0] == '#' {
		// skip comments
		return nil
	} else if len(d.q) == 0 {
		// first token, takes commands instead of tokens?
		if parser.StringInSlice(text, commandOptions) >= 0 {
			d.Split = splitLong
		}
	}

	d.q = append(d.q, text)
	return nil
}

// Reduce removes all unknown interfaces and adjusts the "auto" option
func (d *parserData) Reduce(ifaces []string) bool {
	// reset parsing data
	d.lastIface = nil
	d.Split = splitToken
	d.q = d.q[:0]

	// and make sure interface state is consistent
	if len(ifaces) == 0 {
		ifaces = d.ifaces.Names()
	}
	return d.ifaces.ReduceByName(ifaces)
}

// Generate Interfaces data
func (d *parserData) Export(ifaces []string) (*Interfaces, error) {
	out := d.ifaces.Copy()
	if len(ifaces) > 0 {
		out.ReduceByName(ifaces)
	}

	return &out, nil
}
