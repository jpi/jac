package interfaces // import "go.jpi.io/jac/utils/sysconf/ifupdown/interfaces"

import (
	"go.jpi.io/jac/utils/parser"

	"fmt"
	"net"
	"sort"
)

type Interfaces struct {
	ifaces  map[string]*Iface
	Options ifaceOptions
}

func NewInterfaces() Interfaces {
	return Interfaces{
		ifaces:  make(map[string]*Iface),
		Options: newIfaceOptions(nil),
	}
}

func (c *Interfaces) copyInterface(name string, src *Iface, out *Interfaces) (*Iface, error) {
	if iface, err := out.New(src.Name, src.Family, src.Method); err != nil {
		return nil, err
	} else {
		iface.Auto = src.Auto
		src.Options.CopyTo(&iface.Options)
		return iface, nil
	}
}

func (c *Interfaces) Copy(ifaces ...string) Interfaces {
	out := NewInterfaces()

	if len(ifaces) > 0 {
		for name, p := range c.ifaces {
			if parser.StringInSlice(name, ifaces) < 0 {
				if _, err := c.copyInterface(name, p, &out); err != nil {
					panic(err)
				}
			}
		}
	} else {
		for name, p := range c.ifaces {
			if _, err := c.copyInterface(name, p, &out); err != nil {
				panic(err)
			}
		}
	}

	c.Options.CopyTo(&out.Options)
	return out
}

func (c *Interfaces) Names() []string {
	names := make([]string, len(c.ifaces))[:0]
	for k, _ := range c.ifaces {
		names = append(names, k)
	}

	sort.Strings(names)
	return names
}

//
func (c *Interfaces) New(name, family, method string) (*Iface, error) {
	if p, ok := c.ifaces[name]; ok {
		err := fmt.Errorf("%s: Duplicated Interface", name)
		return p, err
	} else {
		p = NewIface(name, family, method)
		c.ifaces[name] = p
		return p, nil
	}
}

func (c *Interfaces) Get(name string) *Iface {
	if p, ok := c.ifaces[name]; ok {
		return p
	} else {
		return nil
	}
}

// Remove removes an interface if it exists
func (c *Interfaces) Remove(name string) {
	delete(c.ifaces, name)
}

// ReduceByName removes all interfaces not included on the list...
// and applies the `auto` option if present
func (c *Interfaces) ReduceByName(ifaces []string) bool {
	reduced := false

	// remove unknown interfaces
	for name, _ := range c.ifaces {
		if parser.StringInSlice(name, ifaces) < 0 {
			delete(c.ifaces, name)
			reduced = true
		}
	}

	// synchronise `auto` option if set
	if auto, ok := c.GetOption("auto"); ok {
		// reset Auto flag
		for _, p := range c.ifaces {
			p.Auto = false
		}

		// and set Auto flag of those on the `auto` list
		for _, name := range auto {
			if p, ok := c.ifaces[name]; ok {
				p.Auto = true
			}
		}

		// and kill the `auto` option for good
		c.RemoveOption("auto")
	}

	return reduced
}

func (c *Interfaces) Reduce(ifaces []net.Interface) bool {
	names := make([]string, len(ifaces))[:0]
	for _, v := range ifaces {
		names = append(names, v.Name)
	}

	return c.ReduceByName(names)
}

//
func (c *Interfaces) AddOption(key string, values ...string) error {
	return c.Options.Add(key, values...)
}

func (c *Interfaces) SetOption(key string, values ...string) error {
	return c.Options.Set(key, values...)
}

func (c *Interfaces) GetOption(key string) ([]string, bool) {
	v, ok := c.Options.Get(key)
	return v, ok
}

func (c *Interfaces) RemoveOption(key string) {
	c.Options.Remove(key)
}
