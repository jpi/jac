package interfaces // import "go.jpi.io/jac/utils/sysconf/ifupdown/interfaces"

import (
	"io"
	"os"
	"strings"
)

const (
	InterfacesFile = "/etc/network/interfaces"
)

//
func GetInterfaces() (*Interfaces, error) {
	return GetInterfacesFromFile(InterfacesFile)
}

func GetInterfacesFromFile(filename string) (*Interfaces, error) {
	if f, err := os.Open(filename); err != nil {
		return nil, err
	} else {
		defer f.Close()

		return GetInterfacesFromReader(f)
	}
}

func GetInterfacesFromString(input string) (*Interfaces, error) {
	f := strings.NewReader(input)
	return NewParser(f).Run()
}

func GetInterfacesFromReader(f io.Reader) (*Interfaces, error) {
	return NewParser(f).Run()
}

//
func WriteInterfaces(d *Interfaces) error {
	return WriteInterfacesToFile(d, InterfacesFile)
}

func WriteInterfacesToFile(d *Interfaces, filename string) error {
	tmpfile := filename + "~"

	if f, err := os.Create(tmpfile); err != nil {
		return err
	} else {
		defer f.Close()

		if err = WriteInterfacesToWriter(d, f); err == nil {
			err = os.Rename(tmpfile, filename)
		}

		return err
	}
}

func WriteInterfacesToWriter(d *Interfaces, out io.Writer) error {
	_, err := d.Write(out)
	return err
}
