package interfaces

import (
	"bufio"
	"io"
	"strings"
)

func (c *Interfaces) formatLines() []string {
	var out []string

	// loopback first
	if p := c.Get("lo"); p != nil {
		out = append(out, p.Format())
	}
	// then the rest, sorted by name
	for _, name := range c.Names() {
		if name != "lo" {
			if p := c.Get(name); p != nil {
				out = append(out, p.Format())
			}
		}
	}

	return out
}

func (c *Interfaces) Format() string {
	out := c.formatLines()
	return strings.Join(out, "\n")
}

func (c *Interfaces) WriteToBuffer(w *bufio.Writer) (count int, err error) {
	for i, l := range c.formatLines() {
		var n int

		if i > 0 {
			if n, err = w.WriteRune('\n'); err != nil {
				return
			} else {
				count += n
			}
		}

		if n, err = w.WriteString(l); err != nil {
			return
		} else {
			count += n
		}
	}
	return
}

func (c *Interfaces) Write(out io.Writer) (n int, err error) {
	w := bufio.NewWriter(out)

	if n, err = c.WriteToBuffer(w); err == nil {
		err = w.Flush()
	}

	return
}
