package interfaces

import (
	"bufio"
	"strings"
)

func (p *Iface) WriteToBuffer(w *bufio.Writer) (int, error) {
	return w.WriteString(p.Format())
}

func (p *Iface) Format() string {
	var b strings.Builder
	// auto $IFACE
	if p.Auto {
		b.WriteString("auto ")
		b.WriteString(p.Name)
		b.WriteRune('\n')
	}
	// iface $IFACE inet $METHOD
	b.WriteString("iface ")
	b.WriteString(p.Name)
	b.WriteRune(' ')
	b.WriteString(p.Family)
	b.WriteRune(' ')
	b.WriteString(p.Method)
	b.WriteRune('\n')
	// <tab>option value
	for _, k := range p.Options.Names() {
		if values, ok := p.Options.Get(k); ok {
			for _, v := range values {
				if len(v) > 0 {
					b.WriteRune('\t')
					b.WriteString(k)
					b.WriteRune(' ')
					b.WriteString(v)
					b.WriteRune('\n')
				}
			}
		}
	}
	return b.String()
}
