package interfaces

import (
	"go.jpi.io/jac/utils/parser"

	"bytes"
	"unicode"
)

// accept helpers
const (
	space = " \t"
	crlf  = "\r\n"
	lf    = "\n"
)

var (
	byteIsSpace              = parser.NewByteInStringAccepter(space)
	stringIsNewline          = parser.NewStringAccepter(crlf, lf)
	stringIsNewlineOrComment = parser.NewStringAccepter(crlf, lf, "#")
)

// bufio.SplitFunc
func (p *Parser) Split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	for len(data) > 0 && token == nil && err == nil {
		var l int
		var ok bool

		if data[0] == '#' {
			// comment, all the rest of the line
			if l, _, ok = parser.FindStringBytes(data, stringIsNewline); ok {
				token = data[:l]
			} else if atEOF {
				// ... or EOF
				l = len(data)
				token = data
			}
		} else if l, token, ok = parser.AcceptStringBytes(data, stringIsNewline); ok {
			// line break
		} else if l, ok = parser.AcceptManyBytes(data, byteIsSpace); ok {
			// whitespace
		} else {
			// but the rest is up to the state machine
			l, token, err = p.d.Split(data, atEOF)
		}

		advance += l
		data = data[l:]
	}

	return
}

func runeIsToken(c rune) bool {
	return !unicode.IsSpace(c) && c != '#'
}

func splitToken(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if l, _, ok := parser.AcceptTokenBytes(data, atEOF, runeIsToken); ok {
		advance = l
		token = data[:l]
	}

	return
}

func splitLong(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if l, _, ok := parser.FindStringBytes(data, stringIsNewlineOrComment); ok {
		// rest of the line, not including comments
		advance = l
		token = bytes.TrimRight(data[:l], space)
	} else if atEOF {
		advance = len(data)
		token = bytes.TrimRight(data, space)
	}

	return
}
