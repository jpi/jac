package interfaces

type Iface struct {
	Name    string
	Auto    bool
	Family  string
	Method  string
	Options ifaceOptions
}

func NewIface(name, family, method string) *Iface {
	p := &Iface{
		Name:   name,
		Family: family,
		Method: method,
	}

	p.Options = newIfaceOptions(p)
	return p
}

func (p *Iface) AddOption(key string, values ...string) error {
	return p.Options.Add(key, values...)
}

func (c *Iface) SetOption(key string, values ...string) error {
	return c.Options.Set(key, values...)
}

func (c *Iface) GetOption(key string) ([]string, bool) {
	return c.Options.Get(key)
}
