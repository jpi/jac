package hostname

import (
	"go.jpi.io/jac/os/exec"

	"errors"
	"os"
	"strings"
)

func Hostname() (string, error) {
	return os.Hostname()
}

func SetHostname(hostname string) error {
	var b strings.Builder

	// fail immediatelly
	b.WriteString("set -e;")

	// hostname {{.Hostname}};
	cmd := exec.ShellArgs{"hostname", hostname}
	b.WriteString(cmd.String())
	b.WriteRune(';')

	// hostname > /etc/hostname
	b.WriteString("hostname > /etc/hostname")

	// TODO: avoid the shell out
	if err, _, stderr := exec.System(b.String()); err != nil {
		return errors.New(strings.TrimSpace(stderr))
	}

	return nil
}
