package network

import (
	"bufio"
	"fmt"
)

func (p *Interfaces) WritePlainToBuffer(w *bufio.Writer) (count int, err error) {
	for i, name := range p.names {
		if v, ok := p.ifaces[name]; ok {
			var n int

			if i > 0 {
				if n, err = w.WriteRune('\n'); err != nil {
					return
				}
				count += n
			}

			if n, err = v.WritePlainToBuffer(w); err != nil {
				return
			}
			count += n
		}
	}

	return
}

func (p *Interface) WritePlainToBuffer(w *bufio.Writer) (count int, err error) {
	var s string
	var n int

	// header
	if len(p.HwAddress) > 0 {
		s = fmt.Sprintf("# %s (%s)\n#\n", p.Name, p.HwAddress)
	} else {
		s = fmt.Sprintf("# %s\n#\n", p.Name)
	}

	if n, err = w.WriteString(s); err != nil {
		return
	} else {
		count += n
	}

	// content
	if p.ifupdown != nil {
		if n, err = p.ifupdown.WriteToBuffer(w); err != nil {
			return
		} else {
			count += n
		}
	}

	return
}
