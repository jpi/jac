package network // import "go.jpi.io/jac/utils/sysconf/network"

import (
	"go.jpi.io/jac/utils/sysconf/ifupdown/interfaces"

	"fmt"
	"net"
	"strings"
)

type Interface struct {
	Name      string `json:"name"`
	HwAddress string `json:"hwaddr,omitempty"`
	Method    string `json:"method,omitempty"`
	Auto      bool   `json:"auto,omitempty"`

	// manual
	Address string `json:"address,omitempty"`
	Gateway string `json:"gateway,omitempty"`
	Netmask string `json:"netmask,omitempty"`

	iface    *net.Interface
	ifupdown *interfaces.Iface
}

func NewInterface(name string, iface *net.Interface) (*Interface, error) {
	var hwaddress string

	if iface != nil && iface.HardwareAddr != nil {
		// TODO: reimplement without `fmt`
		var b strings.Builder

		for i, x := range iface.HardwareAddr {
			if i > 0 {
				b.WriteRune(':')
			}
			b.WriteString(fmt.Sprintf("%02x", x))
		}

		hwaddress = b.String()
	}

	p := &Interface{
		Name:      name,
		HwAddress: hwaddress,
		iface:     iface,
	}

	return p, nil
}
