package network

import (
	"go.jpi.io/jac/utils/sysconf/ifupdown/interfaces"

	"encoding/json"
	"net"
	"sort"
)

type Interfaces struct {
	names  []string
	ifaces map[string]*Interface

	ifupdown *interfaces.Interfaces
}

func (m *Interfaces) MarshalJSON() ([]byte, error) {
	out := make([]*Interface, len(m.names))[:0]

	for _, name := range m.names {
		out = append(out, m.ifaces[name])
	}

	return json.Marshal(out)
}

func (m *Interfaces) Get(name string) *Interface {
	if v, ok := m.ifaces[name]; ok {
		return v
	} else {
		return nil
	}
}

//
func newInterfaces(ifaces []net.Interface) (*Interfaces, map[string]*net.Interface) {
	names := make([]string, len(ifaces))[:0]
	m := make(map[string]*net.Interface, len(ifaces))

	for i, v := range ifaces {
		// ignore `sit0` and `lo`
		if v.Name != "sit0" && v.Name != "lo" {
			// prepare names list to be sorted
			names = append(names, v.Name)

			// we can't use `&v` because it always has the same address...
			m[v.Name] = &ifaces[i]
		}
	}

	sort.Strings(names)

	p := &Interfaces{
		names:  names,
		ifaces: make(map[string]*Interface),
	}

	return p, m
}

func NewInterfaces(ifaces []net.Interface) (*Interfaces, error) {
	p, m := newInterfaces(ifaces)
	for _, name := range p.names {
		if v, ok := m[name]; ok {
			if q, err := NewInterface(name, v); err != nil {
				return nil, err
			} else {
				p.ifaces[name] = q
			}
		}
	}
	return p, nil
}

func NewInterfacesFromIfupdown(ifaces []net.Interface, conf *interfaces.Interfaces) (*Interfaces, error) {
	p, m := newInterfaces(ifaces)
	for _, name := range p.names {
		if v, ok := m[name]; ok {
			var c *interfaces.Iface

			if conf != nil {
				c = conf.Get(name)
			}

			if q, err := NewInterfaceFromIfupdown(name, v, c); err != nil {
				return nil, err
			} else {
				p.ifaces[name] = q
			}
		}
	}
	return p, nil
}
