package network

import (
	"go.jpi.io/jac/utils/sysconf/ifupdown/interfaces"

	"net"
)

func NewInterfaceFromIfupdown(name string,
	iface *net.Interface,
	ifupdown *interfaces.Iface) (*Interface, error) {

	if p, err := NewInterface(name, iface); err != nil {
		return nil, err
	} else if err = p.initIfupdown(ifupdown); err != nil {
		return nil, err
	} else {
		return p, nil
	}
}

func (p *Interface) initIfupdown(c *interfaces.Iface) error {
	if c != nil {
		p.ifupdown = c
		p.Method = c.Method

		if p.Method == "static" {
			p.Address = p.getIfupdownOption("address")
			p.Gateway = p.getIfupdownOption("gateway")
			p.Netmask = p.getIfupdownOption("netmask")
		}
	}
	return nil
}

func (p *Interface) getIfupdownOption(name string) string {
	var out string

	if v, ok := p.ifupdown.GetOption(name); ok && len(v) > 0 {
		out = v[0]
	}

	return out
}
