# JPI Agent Console #

The JPI Agent Console (aka _jac_) provides generic-_ish_ infrastruture written purely in
Go for central servers where most of the clients are _embedded agents_ initially coming
out of a _manufacturing_ phase and then going into _comissioning_ where they will be
associated with a role and customer.

After comissioning they are _monitored_ and remote _controlled_ by their owner.

## Workspace ##

```sh
mkdir jac-ws
cd jac-ws/

mkdir -p src/go.jpi.io/
git clone ssh://git@bitbucket.org/amery/docker-go-builder docker
git clone ssh://git@bitbucket.org/jpi/jac src/go.jpi.io/jac

ln -s docker/run.sh docker.sh
ln -s docker/top-level.mk docker-go.mk
ln -s src/go.jpi.io/jac/top-level.mk Makefile
make
```
