package server

type Config struct {
	SSHPort          uint16 // sshd port
	SSHKeysDirectory string
	Port             uint16 // https listening port
}

func (c *Config) SetDefaults() {
	c.setSSHDefaults()
}
