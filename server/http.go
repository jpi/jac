package server

import (
	_ "golang.org/x/net/http2"
	"net/http"

	"fmt"
	"net"
)

type HTTPServer struct {
	Logger

	abort chan error
	c     *Config

	srv *http.Server
}

func (s *HTTPServer) ListenAndServe(addr string, c *Config) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	s.Notice("Listening HTTPS at %s", l.Addr())
	s.srv = &http.Server{Addr: addr, Handler: s}

	defer l.Close()
	return s.srv.Serve(l)
}

func (s *HTTPServer) Serve(c *Config) error {
	addr := fmt.Sprintf(":%v", c.Port)
	return s.ListenAndServe(addr, c)
}

func (s *HTTPServer) Terminate(err error) {
	s.Notice("Terminating HTTPS: %s", err)
	s.srv.Close()
	s.abort <- err
}

// Handler
func (s *HTTPServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.NotFound(w, r)
}

// constructor
func NewHTTPServer(c *Config, prefix string) (*HTTPServer, error) {
	s := &HTTPServer{
		abort: make(chan error, 1),
		c:     c,
	}
	s.Logger.addSuffix(prefix)
	return s, nil
}
