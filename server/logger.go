package server

import (
	"go.jpi.io/jac/log"
)

const (
	lNotice log.Variant = iota
	lError
	lDebug
	lTrace
	lTraceError
)

type Logger struct {
	l *log.Logger
}

var logger = &Logger{
	l: log.New("", log.Lor|log.Lrelative|log.Lelapsed|log.Lmicroseconds).
		SetVariant(lError, "ERROR: ", 0).
		SetVariant(lDebug, "DEBUG: ", log.Lor|log.Lfunc).
		SetVariant(lTrace, "TRACE: ", log.Lor|log.Lfunc|log.Lfileline).
		SetVariant(lTraceError, "ERROR: ", log.Lor|log.Lfunc|log.Lfileline),
}

func (l *Logger) addSuffix(suffix string, args ...interface{}) {
	if l.l == nil {
		l.l = logger.l.New(suffix, args...)
	} else {
		l.l.AppendPrefix(suffix, args...)
	}
}

func (l *Logger) prefix(suffix string) string {
	return l.l.Prefix() + suffix
}

func (l *Logger) Notice(fmt string, args ...interface{}) {
	l.l.Outputf(1, lNotice, fmt, args...)
}

func (l *Logger) Error(fmt string, args ...interface{}) {
	l.l.Outputf(1, lNotice, fmt, args...)
}

func (l *Logger) Debug(fmt string, args ...interface{}) {
	l.l.Outputf(1, lDebug, fmt, args...)
}

func (l *Logger) Trace(fmt string, args ...interface{}) {
	l.l.Outputf(1, lTrace, fmt, args...)
}

func (l *Logger) TraceError(fmt string, args ...interface{}) {
	l.l.Outputf(1, lTraceError, fmt, args...)
}
