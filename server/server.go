package server // import "go.jpi.io/jac/server"

import (
	"sync"
)

type server interface {
	Serve(*Config) error
	Terminate(error)
}

type Server struct {
	Logger

	c     *Config
	ret   chan error
	mu    sync.Mutex
	ss    []server
	count int
}

func NewServer(c *Config, prefix string) *Server {
	s := &Server{
		c: c,
	}
	s.Logger.addSuffix(prefix)
	return s
}

// Keep list of running services
func (s *Server) pop(srv server, err error) {
	s.ret <- err

	s.mu.Lock()
	for i, v := range s.ss {
		if v == srv {
			s.ss[i] = nil
		}
	}
	s.count--
	s.mu.Unlock()
}

func (s *Server) spawn(srv server) {
	s.mu.Lock()
	s.ss = append(s.ss, srv)
	s.count++
	s.mu.Unlock()

	go func() {
		s.pop(srv, srv.Serve(s.c))
	}()
}

// Spawn SSH server if enabled
func (s *Server) ServeSSH() error {
	if s.c.SSHPort == 0 {
		// disabled
	} else if srv, err := NewSSHServer(s.c, s.Logger.prefix("ssh: ")); err != nil {
		return err
	} else if srv != nil {
		s.spawn(srv)
	}

	return nil
}

// Spawn HTTP server if enabled
func (s *Server) ServeHTTP() error {
	if s.c.Port == 0 {
		// disabled
	} else if srv, err := NewHTTPServer(s.c, s.Logger.prefix("http: ")); err != nil {
		return err
	} else if srv != nil {
		s.spawn(srv)
	}

	return nil
}

// Run services until they are done
func (s *Server) Serve() error {
	var err error

	// exit aggregator
	s.ret = make(chan error, 1)

	if err = s.ServeSSH(); err != nil {
		s.Error("Failed to start SSH server: %s", err.Error())
	} else if err = s.ServeHTTP(); err != nil {
		s.Error("Failed to start HTTP server: %s", err.Error())
	} else {
		s.Notice("Running...")

		// wait until all servers are done
		for s.count > 0 {
			e := <-s.ret
			if e != nil {
				err = e
			}
		}
	}

	close(s.ret)
	return err
}

// Ask all services to finish
func (srv *Server) Terminate(err error) {
	for _, s := range srv.ss {
		if s != nil {
			s.Terminate(err)
		}
	}
}
