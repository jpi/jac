package server

import (
	"golang.org/x/crypto/ssh"

	"fmt"
	"io/ioutil"
	"net"
	"path/filepath"
)

func (c *Config) setSSHDefaults() {
	if c.SSHPort == 0 {
		c.SSHKeysDirectory = ""
	}
}

type SSHServer struct {
	Logger

	abort chan error
	c     *Config

	config ssh.ServerConfig
}

//
func (s *SSHServer) ListenAndServe(addr string, c *Config) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	s.Logger.addSuffix("%v: ", l.Addr())
	s.Notice("Listening SSH at %v", l.Addr())
	l.Close()
	return nil
}

func (s *SSHServer) Serve(c *Config) error {
	addr := fmt.Sprintf(":%v", c.SSHPort)
	return s.ListenAndServe(addr, c)
}

func (s *SSHServer) Terminate(err error) {
	s.Notice("Terminating SSH: %s", err)
	s.abort <- err
}

// Constructor
func NewSSHServer(c *Config, prefix string) (*SSHServer, error) {
	srv := &SSHServer{
		abort: make(chan error, 1),
		c:     c,
	}
	srv.Logger.addSuffix(prefix)
	pattern := filepath.Join(c.SSHKeysDirectory, "ssh_host_*_key")

	// load keys
	if files, err := filepath.Glob(pattern); err != nil {
		return nil, err
	} else {
		keys := 0

		for _, f := range files {
			key, err := ioutil.ReadFile(f)
			if err != nil {
				srv.Error("%s: %s", f, err.Error())
				continue
			}

			hk, err := ssh.ParsePrivateKey(key)
			if err != nil {
				srv.Error("%s: %s", f, err.Error())
				continue
			}

			pk := hk.PublicKey()
			srv.Notice("%s %s (%s)", pk.Type(), ssh.FingerprintLegacyMD5(pk), f)

			srv.config.AddHostKey(hk)
			keys++
		}

		if keys == 0 {
			err = fmt.Errorf("%q not found", pattern)
			return nil, err
		}
	}

	srv.config.SetDefaults()
	return srv, nil
}
